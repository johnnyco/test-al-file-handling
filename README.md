## Synopsis
***
* Tool to open up various storage formats and to convert between them.
* v0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## Examples
***
* <current_path> \> ```cd <path_to_repo>/src```
* python main.py -q
* python main.py -i ../data/personal.xml -r html
* python main.py -i ../data/personal.xml -o ../output/personal_out.json
* python main.py -i ../data/personal.xml

### Requirements
* Python 2.7 > installed and python.exe can be found from $PATH

## Installation
***

[Download](repo https://bitbucket.org/johnnyco/test-al-file-handling/downloads?tab=tags) or clone - \> ```git clone https://johnnyco@bitbucket.org/johnnyco/test-al-file-handling.git```
1. <current_path> \> ```cd <path_to_repo>/src```
2. <path_to_repo>/src \> ```python main.py -h```